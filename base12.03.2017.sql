-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: dj-system-task
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add priorety',7,'add_priorety'),(20,'Can change priorety',7,'change_priorety'),(21,'Can delete priorety',7,'delete_priorety'),(22,'Can add work bio',8,'add_workbio'),(23,'Can change work bio',8,'change_workbio'),(24,'Can delete work bio',8,'delete_workbio'),(25,'Can add department',9,'add_department'),(26,'Can change department',9,'change_department'),(27,'Can delete department',9,'delete_department'),(28,'Can add status task',10,'add_statustask'),(29,'Can change status task',10,'change_statustask'),(30,'Can delete status task',10,'delete_statustask'),(31,'Can add task',11,'add_task'),(32,'Can change task',11,'change_task'),(33,'Can delete task',11,'delete_task'),(34,'Can add role',12,'add_role'),(35,'Can change role',12,'change_role'),(36,'Can delete role',12,'delete_role'),(37,'Can add post',13,'add_post'),(38,'Can change post',13,'change_post'),(39,'Can delete post',13,'delete_post');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$30000$qkjWf127mcip$qYmQyDVGVPkWyAdlU7UkvCn8+nxjTf/gyMOK2/3wMtY=','2017-03-11 14:14:20.363519',1,'admin','Системный','Администратор','123@localhost',1,1,'2017-03-11 13:53:17.000000'),(2,'pbkdf2_sha256$30000$xQV1BYwn9Q50$gpEHDPOjQhBpCfX8iSg6DxrWs8W2lUDU9/OkpufYMN8=','2017-03-11 14:14:58.092950',0,'tarabrinm','Максим','Тарабрин','xbox1995@yandex.ru',0,1,'2017-03-11 13:58:41.000000');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2017-03-11 13:56:27.796398','1','Руководитель',1,'[{\"added\": {}}]',3,1),(2,'2017-03-11 13:56:35.028666','2','Подчиненный',1,'[{\"added\": {}}]',3,1),(3,'2017-03-11 13:56:44.113895','2','Подчиненный',3,'',3,1),(4,'2017-03-11 13:56:44.143381','1','Руководитель',3,'',3,1),(5,'2017-03-11 13:56:57.468880','1','Руководитель',1,'[{\"added\": {}}]',12,1),(6,'2017-03-11 13:57:05.971300','1',' ',1,'[{\"added\": {}}]',8,1),(7,'2017-03-11 13:57:16.536764','1','Администратор Системный',2,'[{\"changed\": {\"fields\": [\"first_name\", \"last_name\"]}}]',4,1),(8,'2017-03-11 13:57:46.269802','1','Отдел внедрения и сопровождения информационных систем и баз данных',1,'[{\"added\": {}}]',9,1),(9,'2017-03-11 13:57:52.632992','1','Ведущий специалист-эксперт',1,'[{\"added\": {}}]',13,1),(10,'2017-03-11 13:58:09.698805','1','Администратор Системный',2,'[{\"changed\": {\"fields\": [\"department\", \"post\"]}}]',8,1),(11,'2017-03-11 13:58:41.299586','2',' ',1,'[{\"added\": {}}]',4,1),(12,'2017-03-11 13:58:50.449386','2','Тарабрин Максим',2,'[{\"changed\": {\"fields\": [\"first_name\", \"last_name\", \"email\"]}}]',4,1),(13,'2017-03-11 13:59:28.960923','2','Подчинённый',1,'[{\"added\": {}}]',12,1),(14,'2017-03-11 13:59:34.057751','2','Тарабрин Максим',1,'[{\"added\": {}}]',8,1),(15,'2017-03-11 14:01:48.818073','1','Низкий',1,'[{\"added\": {}}]',7,1),(16,'2017-03-11 14:01:52.551710','2','Средний',1,'[{\"added\": {}}]',7,1),(17,'2017-03-11 14:01:56.019092','3','Высокий',1,'[{\"added\": {}}]',7,1),(18,'2017-03-11 14:07:17.784900','1','Не обработанные',1,'[{\"added\": {}}]',10,1),(19,'2017-03-11 14:07:21.430980','2','В работе',1,'[{\"added\": {}}]',10,1),(20,'2017-03-11 14:07:26.467137','3','Выполнено',1,'[{\"added\": {}}]',10,1),(21,'2017-03-11 14:07:47.974456','5','Привет мир!',1,'[{\"added\": {}}]',11,1),(22,'2017-03-11 14:14:33.291604','4','Отложено',1,'[{\"added\": {}}]',10,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(9,'mainApp','department'),(13,'mainApp','post'),(7,'mainApp','priorety'),(12,'mainApp','role'),(10,'mainApp','statustask'),(11,'mainApp','task'),(8,'mainApp','workbio'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2017-03-11 13:50:40.211236'),(2,'auth','0001_initial','2017-03-11 13:50:46.165226'),(3,'admin','0001_initial','2017-03-11 13:50:48.102455'),(4,'admin','0002_logentry_remove_auto_add','2017-03-11 13:50:48.153475'),(5,'contenttypes','0002_remove_content_type_name','2017-03-11 13:50:48.994568'),(6,'auth','0002_alter_permission_name_max_length','2017-03-11 13:50:49.823672'),(7,'auth','0003_alter_user_email_max_length','2017-03-11 13:50:50.306750'),(8,'auth','0004_alter_user_username_opts','2017-03-11 13:50:50.345250'),(9,'auth','0005_alter_user_last_login_null','2017-03-11 13:50:50.710798'),(10,'auth','0006_require_contenttypes_0002','2017-03-11 13:50:50.741306'),(11,'auth','0007_alter_validators_add_error_messages','2017-03-11 13:50:50.777292'),(12,'auth','0008_alter_user_username_max_length','2017-03-11 13:50:51.281384'),(13,'mainApp','0001_initial','2017-03-11 13:50:51.307876'),(14,'mainApp','0002_auto_20170311_1711','2017-03-11 13:50:51.331876'),(15,'mainApp','0003_auto_20170311_1712','2017-03-11 13:50:58.743655'),(16,'mainApp','0004_auto_20170311_1716','2017-03-11 13:50:58.802178'),(17,'sessions','0001_initial','2017-03-11 13:50:59.294242');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('1f6ouvwjkxq8mf3bl008w94xuuitmsiz','ZWQyOGVhY2YyNDhhNzdiMjEyNWRhOTFjZWY4ZWRlNDBhNDYxMDhlODp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjIiLCJfYXV0aF91c2VyX2hhc2giOiI1YTczODEwNDAwNGY4ZGY3MDFjNDFjZDhlZGRjNGE4ZWU3NThlMWJjIn0=','2017-03-25 14:14:58.145441'),('mk05q7tnuc4d29203d3zraxtasr5n0kb','OTZmNmI0OTIxZWMxM2JmNGU5MTNhMzdkMmFiYTczYWJjNGM3MjM3Mjp7fQ==','2017-03-25 13:53:50.416144');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mainapp_department`
--

DROP TABLE IF EXISTS `mainapp_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mainapp_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `DepartmentName` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mainapp_department`
--

LOCK TABLES `mainapp_department` WRITE;
/*!40000 ALTER TABLE `mainapp_department` DISABLE KEYS */;
INSERT INTO `mainapp_department` VALUES (1,'Отдел внедрения и сопровождения информационных систем и баз данных');
/*!40000 ALTER TABLE `mainapp_department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mainapp_post`
--

DROP TABLE IF EXISTS `mainapp_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mainapp_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `PostName` varchar(50) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mainApp_post_department_id_9b0f33e8_fk_mainApp_department_id` (`department_id`),
  CONSTRAINT `mainApp_post_department_id_9b0f33e8_fk_mainApp_department_id` FOREIGN KEY (`department_id`) REFERENCES `mainapp_department` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mainapp_post`
--

LOCK TABLES `mainapp_post` WRITE;
/*!40000 ALTER TABLE `mainapp_post` DISABLE KEYS */;
INSERT INTO `mainapp_post` VALUES (1,'Ведущий специалист-эксперт',1);
/*!40000 ALTER TABLE `mainapp_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mainapp_priorety`
--

DROP TABLE IF EXISTS `mainapp_priorety`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mainapp_priorety` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namePriorety` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mainapp_priorety`
--

LOCK TABLES `mainapp_priorety` WRITE;
/*!40000 ALTER TABLE `mainapp_priorety` DISABLE KEYS */;
INSERT INTO `mainapp_priorety` VALUES (1,'Низкий'),(2,'Средний'),(3,'Высокий');
/*!40000 ALTER TABLE `mainapp_priorety` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mainapp_role`
--

DROP TABLE IF EXISTS `mainapp_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mainapp_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nameRole` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mainapp_role`
--

LOCK TABLES `mainapp_role` WRITE;
/*!40000 ALTER TABLE `mainapp_role` DISABLE KEYS */;
INSERT INTO `mainapp_role` VALUES (1,'Руководитель'),(2,'Подчинённый');
/*!40000 ALTER TABLE `mainapp_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mainapp_statustask`
--

DROP TABLE IF EXISTS `mainapp_statustask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mainapp_statustask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nameStatus` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mainapp_statustask`
--

LOCK TABLES `mainapp_statustask` WRITE;
/*!40000 ALTER TABLE `mainapp_statustask` DISABLE KEYS */;
INSERT INTO `mainapp_statustask` VALUES (1,'Не обработанные'),(2,'В работе'),(3,'Выполнено'),(4,'Отложено');
/*!40000 ALTER TABLE `mainapp_statustask` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mainapp_task`
--

DROP TABLE IF EXISTS `mainapp_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mainapp_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nameTask` varchar(100) NOT NULL,
  `briefDescr` varchar(150) NOT NULL,
  `description` longtext NOT NULL,
  `date_added` datetime(6) NOT NULL,
  `date_deadline` date DEFAULT NULL,
  `priorety_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `userAssign_id` int(11),
  `userInitial_id` int(11),
  PRIMARY KEY (`id`),
  KEY `mainApp_task_priorety_id_bdea515f_fk_mainApp_priorety_id` (`priorety_id`),
  KEY `mainApp_task_status_id_e7946ab3_fk_mainApp_statustask_id` (`status_id`),
  KEY `mainApp_task_userAssign_id_3421ed2b_fk_auth_user_id` (`userAssign_id`),
  KEY `mainApp_task_userInitial_id_af81ce9d_fk_auth_user_id` (`userInitial_id`),
  CONSTRAINT `mainApp_task_priorety_id_bdea515f_fk_mainApp_priorety_id` FOREIGN KEY (`priorety_id`) REFERENCES `mainapp_priorety` (`id`),
  CONSTRAINT `mainApp_task_status_id_e7946ab3_fk_mainApp_statustask_id` FOREIGN KEY (`status_id`) REFERENCES `mainapp_statustask` (`id`),
  CONSTRAINT `mainApp_task_userAssign_id_3421ed2b_fk_auth_user_id` FOREIGN KEY (`userAssign_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `mainApp_task_userInitial_id_af81ce9d_fk_auth_user_id` FOREIGN KEY (`userInitial_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mainapp_task`
--

LOCK TABLES `mainapp_task` WRITE;
/*!40000 ALTER TABLE `mainapp_task` DISABLE KEYS */;
INSERT INTO `mainapp_task` VALUES (5,'Привет мир!','Привет мир!','Привет мир!Привет мир!','2017-03-11 14:07:47.973970',NULL,1,2,2,1),(6,'Задача от самого себя тест','Задача от самого себя тестЗадача от самого себя те...','Задача от самого себя тестЗадача от самого себя тестЗадача от самого себя тест','2017-03-11 14:08:17.381135',NULL,1,3,2,2),(7,'Задача среднего приоритета','Задача среднего приоритетаЗадача среднего приорите...','Задача среднего приоритетаЗадача среднего приоритета','2017-03-11 14:10:57.432585',NULL,2,1,2,2);
/*!40000 ALTER TABLE `mainapp_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mainapp_workbio`
--

DROP TABLE IF EXISTS `mainapp_workbio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mainapp_workbio` (
  `user_id` int(11) NOT NULL,
  `middleName` varchar(50) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `mainApp_workbio_department_id_c9519768_fk_mainApp_department_id` (`department_id`),
  KEY `mainApp_workbio_post_id_fb262051_fk_mainApp_post_id` (`post_id`),
  KEY `mainApp_workbio_role_id_c28df918_fk_mainApp_role_id` (`role_id`),
  CONSTRAINT `mainApp_workbio_department_id_c9519768_fk_mainApp_department_id` FOREIGN KEY (`department_id`) REFERENCES `mainapp_department` (`id`),
  CONSTRAINT `mainApp_workbio_post_id_fb262051_fk_mainApp_post_id` FOREIGN KEY (`post_id`) REFERENCES `mainapp_post` (`id`),
  CONSTRAINT `mainApp_workbio_role_id_c28df918_fk_mainApp_role_id` FOREIGN KEY (`role_id`) REFERENCES `mainapp_role` (`id`),
  CONSTRAINT `mainApp_workbio_user_id_026815f5_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mainapp_workbio`
--

LOCK TABLES `mainapp_workbio` WRITE;
/*!40000 ALTER TABLE `mainapp_workbio` DISABLE KEYS */;
INSERT INTO `mainapp_workbio` VALUES (1,'Админович',1,1,1),(2,'Олегович',1,1,2);
/*!40000 ALTER TABLE `mainapp_workbio` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-12 13:52:45
