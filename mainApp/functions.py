from django.core.mail import EmailMultiAlternatives

def func_mail_send(new_task=None):
    if new_task is not None:
        mail_name = '#' + str(new_task.id) + '. ' + str(new_task.nameTask)
        if new_task.date_deadline is None:
            email_date_deadline = 'Не указано'
        else:
            email_date_deadline = new_task.date_deadline
        mail_message = '<p>У вас имеется новое задание!</p>' \
                       '<p><strong>Название: </strong> ' + str(new_task.nameTask) + '<br>' + \
                       '<strong>Описание задачи: </strong>' + str(new_task.description) + '<br>' + \
                       '<strong>Приоритет: </strong>' + str(new_task.priorety) + '<br>' + \
                       '<strong>Назначил: </strong>' + str(new_task.userInitial) + '<br>' + \
                       '<strong>Крайник срок: </strong>' + str(email_date_deadline) + '<br>' + \
                       '<a href="http://10.77.99.49/tasks/' + str(new_task.id) + '">Ссылка на задание #' + \
                       str(new_task.id) + '</a></p>' + \
                       '<p>______________________________________________________________________<br>' + \
                       'Это информационное письмо, отвечать на него не нужно!' + '</p>'
        subject, from_email, to = mail_name, 'Система_задач_ОПФР', new_task.userAssign.email
        text_content = 'This is an important message.'
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(mail_message, "text/html")
        msg.send()
    else:
        print('Ошибка отправления письма')

def func_notification_send(task=None):
    mail_message = '<p>У вас имеется задание срочной важности:</p>' \
                   '<p><strong>Название: </strong> ' + str(task.nameTask) + '<br>' + \
                   '<a href="http://10.77.99.49/tasks/' + str(task.id) + '">Ссылка на задание #' + \
                   str(task.id) + '</a></p>' + \
                   '<p>______________________________________________________________________<br>' + \
                   'Это информационное письмо, отвечать на него не нужно!' + '</p>'
    subject, from_email, to = "[Важно] "+task.nameTask, 'Система_задач_ОПФР', task.userAssign.email
    text_content = 'This is an important message.'
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(mail_message, "text/html")
    msg.send()