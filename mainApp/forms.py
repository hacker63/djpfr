from django import forms
from django.forms import ModelForm
from .models import User,WorkBio, Task
import datetime

class loginForm(forms.Form):
    login = forms.CharField(max_length=100)
    password = forms.PasswordInput()


class workBioForm(ModelForm):
    class Meta:
        model = WorkBio
        fields = ('middleName','post')

class UserRegistrationForm(forms.ModelForm):
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Повторите пароль', widget=forms.PasswordInput)
    first_name = forms.CharField(label='Имя', required=True)
    email = forms.CharField(label='Электронная почта',required=True)
    last_name = forms.CharField(label='Фамилия',required=True)
    class Meta:
        model = User
        fields = ('username', 'first_name','last_name', 'email')
    def clean_password2(self):
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError('Пароли не совпадают.')
        return cd['password2']

class taskForm(forms.ModelForm):
    date_deadline = forms.DateField(label = 'Крайний срок выполнения',
                                    required = False,
                                    widget=forms.TextInput(attrs={
                                        'placeholder':datetime.date.today().strftime('%d.%m.%Y'),
                                        'required':False}))
    class Meta:
        model = Task
        fields = ('nameTask','description','date_deadline','priorety')