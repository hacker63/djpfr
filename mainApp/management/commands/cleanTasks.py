from django.core.management.base import BaseCommand, CommandError
from mainApp.models import Task
import datetime

class Command(BaseCommand):
    help = 'Задача очистки устаревших задач'
    def handle(self, *args, **options):
        date_now = datetime.datetime.today()
        numberDays = datetime.timedelta(days=30)
        dateStarting = date_now-numberDays
        tasks = Task.objects.exclude(date_closed__gte=dateStarting).exclude(date_closed__isnull=True).delete()
        self.stdout.write(self.style.SUCCESS('Успешно удалено задач: '+str(tasks[0])))