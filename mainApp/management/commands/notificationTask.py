from django.core.management.base import BaseCommand, CommandError
from mainApp.models import Task
from mainApp.functions import func_notification_send
import datetime

class Command(BaseCommand):
    help = 'Задача оповещение о важныз задачах'
    def handle(self, *args, **options):
        tasks = Task.objects.filter(status=1,priorety=3)
        for task in tasks:
            func_notification_send(task)