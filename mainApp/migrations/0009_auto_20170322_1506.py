# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-22 11:06
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mainApp', '0008_auto_20170322_1503'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='task',
            options={'permissions': (('view_department_task', 'Возможность просматривать все задачи отдела'),)},
        ),
    ]
