$(function() {

	// Custom JS
	$('.cb-visible-pass').is(function(){
			$('.cb-visible-pass').on('click',function(){
				if($('.cb-visible-pass').prop('checked')){
					$("#id_password").prop('type','text')
					$("#id_password2").prop('type','text')
				}else{
					$("#id_password").prop('type','password')
					$("#id_password2").prop('type','password')
				}
			})
	})
	
	$(window).ready(function(){
		// Плавное появление страницы
		$('body').show('slow');
		$('a#popup').click( function(event){ 
			event.preventDefault();
			params = event.currentTarget.toString();
			$('#commentForm').attr('action',params.split('?')[0]);
			params = params.split('?')[1];
			params = params.split('&');
			for(var i = 0; i < params.length;i++){
				splitList = params[i].split('=');
				//alert(splitList[1]);
				jqText = "#"+splitList[0].toString()+"Input";
				$(jqText).attr('value',splitList[1]);
			}
			$('#overlay').fadeIn(250,
								 function(){
									$('#popUp')
										.css('display', 'block')
										.animate({opacity: 1, top: '50%'}, 490);
			});
		});
		/*по нажатию на крестик закрываю окно*/
		$('#close, #overlay').click( function(){
			$('#taskInput').attr('value','');
			$('#actInput').attr('value','');
			$('.commentForm #actInput').attr('value','');
			$('#popUp')
				.animate({opacity: 0, top: '35%'}, 490, 
					function(){ 
						$(this).css('display', 'none'); 
						$('#overlay').fadeOut(220); 
					}
				);
		});
	});
});