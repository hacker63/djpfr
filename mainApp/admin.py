from django.contrib import admin
from .models import Department, Post, Priorety, Task, StatusTask,Role,WorkBio

# Register your models here.
admin.site.register(Department)
admin.site.register(Post)
admin.site.register(Priorety)
admin.site.register(Task)
admin.site.register(StatusTask)
admin.site.register(Role)
admin.site.register(WorkBio)
