from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from mainApp.functions import func_mail_send
from .models import Task,WorkBio,Role,StatusTask
from .forms import workBioForm, UserRegistrationForm, taskForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import datetime
from django.http import Http404
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist

# Create your views here.

def index(request):
    if request.method == 'GET':
        if 'act' in request.GET:
            act = request.GET['act']
            if act == 'exit':
                logout(request)
                return HttpResponseRedirect('/index')
    if request.user.is_authenticated():
       return HttpResponseRedirect('/workspace')
    else:
        if request.method == 'POST':
            # create a form instance and populate it with data from the request:
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                # Redirect to a success page.
                if request.method == 'GET':
                    if request.GET['next']:
                        return HttpResponseRedirect(request.GET['next'])
                return HttpResponseRedirect('/workspace')
            else:
                try:
                    u_err = User.objects.get(username=username)
                    if (u_err.is_active is False):
                        context = {'form': AuthenticationForm(),
                                   'error': 'Ваш аккаунт не активен. Обратитель к системному администратору.',
                                   'VERSION_SYSTEM': settings.VERSION_SYSTEM}
                    else:
                        context = {'form': AuthenticationForm(),
                                   'error': 'Неверный логин или пароль.',
                                   'VERSION_SYSTEM': settings.VERSION_SYSTEM}
                except ObjectDoesNotExist:
                    context = {'form': AuthenticationForm(),
                               'error': 'Неверный логин или пароль.',
                               'VERSION_SYSTEM': settings.VERSION_SYSTEM}
                return render(request, 'default/auth/index.html', context)
        else:
            context = {'form': AuthenticationForm(),'title':'Страница авторизации','VERSION_SYSTEM':settings.VERSION_SYSTEM}
            return render(request, 'default/auth/index.html', context)

@login_required
def workspace(request):
    u = User.objects.get(username=request.user.username)
    if request.method == 'GET':
        if 'act' in request.GET:
            act = request.GET['act']
            if act == 'status':
                if 'task' in request.GET:
                    if request.GET['task'].isdigit():
                        id_task = request.GET['task']
                        task = Task.objects.get(id=id_task)
                        if task.userAssign == u:
                            if task.status.id < 3:
                                if task.status.id+1 == 3:
                                    Task.objects.filter(id=task.id).update(status=task.status.id + 1,
                                                                           date_closed=datetime.datetime.now())
                                else:
                                    Task.objects.filter(id=task.id).update(status=task.status.id + 1)
                            elif task.status.id == 4:
                                Task.objects.filter(id=task.id).update(status=2,comment_return=None)
                            return HttpResponseRedirect('/workspace')
                        else:
                            return HttpResponseRedirect('/workspace')
                    else:
                        return HttpResponseRedirect('/workspace')
                else:
                    return HttpResponse('/workspace')
            elif act == 'return':
                if 'task' in request.GET and 'act' in request.GET and 'new_comment' in request.GET:
                    if request.GET['task'].isdigit():
                        id_task = request.GET['task']
                        task = Task.objects.get(id=id_task)
                        if task.userAssign == u:
                            if task.status.id != 4:
                                Task.objects.filter(id=task.id).update(status=4,date_closed=None,comment_return=request.GET['new_comment'])
                                return HttpResponseRedirect('/workspace')
                            else:
                                return HttpResponseRedirect('/workspace')
                        else:
                            return HttpResponseRedirect('/workspace')
                    else:
                        return HttpResponseRedirect('/workspace')
                else:
                    return HttpResponseRedirect('/workspace')
    if u.workbio.role.id == 2:
        tasks_1 = Task.objects.order_by('-priorety','-date_added').filter(userAssign=u.id,status=1)
        tasks_2 = Task.objects.order_by('-priorety','-date_added').filter(userAssign=u.id,status=2)
        tasks_3 = Task.objects.order_by('-date_closed').filter(userAssign=u.id,status=3)[:10]
        tasks_4 = Task.objects.order_by('-priorety','-date_added').filter(userAssign=u.id, status=4)
        context = {'title':'Рабочее пространство сотрудника','task_1':tasks_1,'task_2':tasks_2,'task_3':tasks_3,
                   'task_4':tasks_4,'myUser':u,'VERSION_SYSTEM':settings.VERSION_SYSTEM}
        return render(request,'default/mainPages/index-empl.html',context)
    elif u.workbio.role.id == 1:
        wb_list = WorkBio.objects.filter(department=u.workbio.department,role=2)
        employes = {}
        tasks = {}
        for wb in wb_list:
            u_wb = User.objects.get(username=wb.user.username)
            if(u_wb.is_active):
                employes[u_wb.username] = u_wb
                tasks[u_wb.username] = Task.objects.order_by('status','-priorety','-date_added').filter(userAssign=wb.user)
        context = {'title':'Рабочее пространство руководителя','employes':employes,'tasks':tasks,'myUser':u,'VERSION_SYSTEM':settings.VERSION_SYSTEM}
        return render(request,'default/mainPages/index.html',context)
    else:
        return render(request,'default/mainPages/forbidden.html',context='')

@login_required
def my_workspace(request):
    u = User.objects.get(username=request.user.username)
    if request.method == 'GET':
        if 'act' in request.GET:
            act = request.GET['act']
            if act == 'status':
                if 'task' in request.GET:
                    if request.GET['task'].isdigit():
                        id_task = request.GET['task']
                        task = Task.objects.get(id=id_task)
                        if task.userAssign == u:
                            if task.status.id < 3:
                                if task.status.id+1 == 3:
                                    Task.objects.filter(id=task.id).update(status=task.status.id + 1,
                                                                           date_closed=datetime.datetime.now(),
                                                                           comment_return=None)
                                else:
                                    Task.objects.filter(id=task.id).update(status=task.status.id + 1)
                            elif task.status.id == 4:
                                Task.objects.filter(id=task.id).update(status=2,
                                                                       comment_return=None)
                            return HttpResponseRedirect('/my_workspace')
                        else:
                            return HttpResponseRedirect('/my_workspace')
                    else:
                        return HttpResponseRedirect('/my_workspace')
                else:
                    return HttpResponse('/my_workspace')
            elif act == 'return':
                if 'task' in request.GET and 'new_comment' in request.GET:
                    if request.GET['task'].isdigit():
                        id_task = request.GET['task']
                        task = Task.objects.get(id=id_task)
                        if task.userAssign == u:
                            if task.status.id != 4:
                                Task.objects.filter(id=task.id).update(status=4,comment_return=request.GET['new_comment'])
                                return HttpResponseRedirect('/my_workspace')
                            else:
                                return HttpResponseRedirect('/my_workspace')
                        else:
                            return HttpResponseRedirect('/my_workspace')
                    else:
                        return HttpResponseRedirect('/my_workspace')
                else:
                    return HttpResponse('/my_workspace')
    if u.workbio.role.id == 1:
        task_1 = Task.objects.order_by('status','-priorety').filter(userInitial=u,userAssign=u,status=1)
        task_2 = Task.objects.order_by('status','-priorety').filter(userInitial=u,userAssign=u,status=2)
        task_3 = Task.objects.order_by('status','-priorety').filter(userInitial=u,userAssign=u,status=3)
        task_4 = Task.objects.order_by('status','-priorety').filter(userInitial=u,userAssign=u,status=4)
        return render(request,'default/mainPages/my_workspace.html',context={'title':'Задачи руководителя',
                                                                             'myUser':u,
                                                                             'task_1':task_1,
                                                                             'task_2':task_2,
                                                                             'task_3':task_3,
                                                                             'task_4':task_4,
                                                                             'VERSION_SYSTEM':settings.VERSION_SYSTEM})
    else:
        return HttpResponseRedirect('/workspace')

@login_required
def users(request,page=1):
    u = request.user
    if request.method == 'GET':
        if 'page' in request.GET:
            page = int(request.GET['page'])
    if u.workbio.role.id == 1:
        user_list = WorkBio.objects.filter(department=u.workbio.department, role=2, ).order_by('-user')
        p = Paginator(user_list, 10)
        try:
            user_list = p.page(page)
        except PageNotAnInteger:
            # Если переменная не integet, то перебрасывает на первую страницу
            user_list = p.page(1)
        except EmptyPage:
            # Если страница вне диапазона (например 9999), поставить последнюю страницу результатов.
            user_list = p.page(p.num_pages)
        context = {'title': 'Список сотрудников', 'user_list': user_list, 'myUser': u,'VERSION_SYSTEM':settings.VERSION_SYSTEM}
        return render(request, 'default/mainPages/user-list.html', context)
    else:
        return HttpResponse('Forbidden')

@login_required
def user(request,user=None):
    u = request.user
    u_id = int(user)
    u_get = User.objects.get(id=u_id)
    user_form = UserRegistrationForm(instance=u_get)
    workbio_form = workBioForm(instance=u_get.workbio)
    if user is not None:
        if (u_id == u.id) or (u.workbio.role.id == 1 and u_get.workbio.department == u.workbio.department):
            if request.method == 'POST':
                user_form = UserRegistrationForm(request.POST)
                workbio_form = workBioForm(request.POST)
                if user_form.is_valid() and workbio_form.is_valid():
                    new_user = user_form.save(commit=False)
                    new_user.save()
                    workbio = workbio_form.save(commit=False)
                    workbio.user = new_user
                    workbio.department = u.workbio.department
                    workbio.save()
                    return HttpResponseRedirect('/workspace')
                else:
                    user_form = UserRegistrationForm(request.POST)
                    workbio_form = workBioForm(request.POST)
                    context = {'title': 'Учётная запись', 'myUser': u, 'user_form': user_form,
                                   'workbio_form': workbio_form,'VERSION_SYSTEM':settings.VERSION_SYSTEM}
                    return render(request, 'default/mainPages/edit-account.html', context)
            context = {'title': 'Учётная запись', 'myUser': u, 'user_form': user_form, 'workbio_form': workbio_form,
                           'u_get': u_get, 'VERSION_SYSTEM': settings.VERSION_SYSTEM}
            return render(request, 'default/mainPages/myAcc.html', context)
        else:
            return HttpResponse('Forbidden')
    else:
        return HttpResponseRedirect('/workspace')

@login_required
def userEdit(request,user=None):
    u = request.user
    if user is not None:
        try:
            user_get = User.objects.get(id=user)
            if u.workbio.role.id == 1 and user_get.workbio.department == u.workbio.department:
                if request.method == 'POST':
                    user_form = UserRegistrationForm(request.POST, instance=user_get)
                    workbio_form = workBioForm(request.POST, instance=user_get.workbio)
                    if user_form.is_valid() and workbio_form.is_valid():
                        # Create a new user object but avoid saving it yet
                        new_user = user_form.save(commit=False)
                        new_user.set_password(user_form.cleaned_data['password'])
                        new_user.save()
                        workbio = workbio_form.save(commit=False)
                        workbio.save()
                        return HttpResponseRedirect('/users/' + str(user_get.id))
                    context = {'title': 'Редактировать учётную запись', 'myUser': u, 'user_form': user_form,
                               'workbio_form': workbio_form, 'u_get': user_get,
                               'VERSION_SYSTEM': settings.VERSION_SYSTEM}
                    return render(request, 'default/mainPages/edit-account.html', context)
                user_form = UserRegistrationForm(instance=user_get)
                workbio_form = workBioForm(instance=user_get.workbio)
                context = {'title': 'Учётная запись', 'myUser': u, 'user_form': user_form,
                           'workbio_form': workbio_form, 'u_get': user_get, 'VERSION_SYSTEM': settings.VERSION_SYSTEM}
                return render(request, 'default/mainPages/edit-account.html', context)
            else:
                return HttpResponseRedirect('/workspace')
        except ObjectDoesNotExist:
            return Http404
    else:
        return HttpResponseRedirect('/workspace')

@login_required
def tasks(request,page=1):
    u = request.user
    if request.method == 'GET':
        if 'page' in request.GET:
            page = int(request.GET['page'])
    # View для руководителя
    if u.workbio.role.id == 1:
            tasks = Task.objects.order_by('status', '-priorety', '-date_added', '-date_deadline').filter(userInitial=u)
    # View для подчиненного
    elif u.workbio.role.id == 2:
        tasks = Task.objects.order_by('status','-priorety','-date_added','-date_deadline').filter(userAssign=u)
    p = Paginator(tasks,10)
    try:
        tasks = p.page(page)
    except PageNotAnInteger:
        # Если переменная не integet, то перебрасывает на первую страницу
        tasks = p.page(1)
    except EmptyPage:
        # Если страница вне диапазона (например 9999), поставить последнюю страницу результатов.
        tasks = p.page(p.num_pages)
    context = {'title': 'Список задач', 'myUser': u, 'tasks': tasks,'VERSION_SYSTEM':settings.VERSION_SYSTEM}
    return render(request, 'default/mainPages/task-list.html', context)

@login_required
def task(request, task=None):
    u = request.user
    taskId = int(task)
    task = Task.objects.get(id=taskId)
    if request.method == 'POST':
        task_form = taskForm(request.POST)
        if task_form.is_valid() and request.POST['userAssign'].isdecimal():
            # Create a new user object but avoid saving it yet
            new_task = task_form.save(commit=False)
            new_task.userInitial = u
            new_task.userAssign = User.objects.get(id=request.POST['userAssign'])
            new_task.briefDescr = new_task.description[0:50] + '...'
            new_task.date_added = datetime.datetime.now()
            # Save the User object
            new_task.save()
            return HttpResponseRedirect('/tasks')
        else:
            employes = WorkBio.objects.filter(department=u.workbio.department, role=2)
            task_form = taskForm(request.POST)
    if (task.userAssign.id == u.id) or (u.workbio.role.id == 1 and u.workbio.department == task.userAssign.workbio.department):
        return render(request,'default/mainPages/task.html',context={'title':'Просмотр задачи','myUser':u,'task':task,'VERSION_SYSTEM':settings.VERSION_SYSTEM})
    else:
        return HttpResponse('Forbidden')

@login_required
def taskEdit(request,task=None):
    u = request.user
    if user is not None:
        task_get = Task.objects.get(id=task)
        tasks_get_userAssign = task_get.userAssign
        if u.workbio.role.id == 1 and\
                (task_get.userInitial == u or task_get.userInitial.workbio.department == u.workbio.department)\
                and task_get.status != 3:
            if u.workbio.role == 2:
                employes = WorkBio.objects.filter(department=u.workbio.department, role=2)
            else:
                employes = WorkBio.objects.filter(department=u.workbio.department)
            if request.method == 'POST':
                task_form = taskForm(request.POST,instance=task_get)
                if task_form.is_valid():
                    task_edit = task_form.save(commit=False)
                    task_edit.userAssign = User.objects.get(id=request.POST['userAssign'])
                    task_edit.status = StatusTask.objects.get(id=1)
                    task_edit.save()
                    if tasks_get_userAssign != task_edit.userAssign:
                        func_mail_send(Task.objects.get(id=task_edit.id))
                    return HttpResponseRedirect('/tasks/'+str(task_get.id))
                else:
                    context = {'title': 'Редактировать учётную запись',
                               'myUser': u,
                               'task_form': task_form,
                               'task': task_get,
                               'employes': employes}
                    return render(request, 'default/mainPages/edit-task.html', context)
                context = {'title': 'Редактировать учётную запись', 'myUser': u,'task_form': task_form,'task':task_get,
                           'employes':employes}
                return render(request, 'default/mainPages/edit-task.html', context)
            task_form = taskForm(instance=task_get)
            context = {'title': 'Редактировать учётную запись', 'myUser': u, 'task_form': task_form,'task':task_get,
                       'employes': employes,'VERSION_SYSTEM':settings.VERSION_SYSTEM}
            return render(request, 'default/mainPages/edit-task.html', context)
        else:
            return HttpResponseRedirect('/tasks/'+str(task))
    else:
        return HttpResponseRedirect('/workspace')

@login_required
def taskNew(request):
    u = request.user
    if request.method == 'POST':
        task_form = taskForm(request.POST)
        if u.workbio.role.id == 1:
            if task_form.is_valid() and request.POST['userAssign'].isdecimal():
                new_task = task_form.save(commit=False)
                new_task.userInitial = u
                new_task.userAssign = User.objects.get(id=request.POST['userAssign'])
                new_task.briefDescr = new_task.description[0:50]
                new_task.briefDescr.strip()
                new_task.briefDescr = new_task.briefDescr
                new_task.date_added = datetime.datetime.now()
                new_task.save()
                func_mail_send(new_task)
                if new_task.userAssign == u:
                    return HttpResponseRedirect("/my_workspace")
                else:
                    return HttpResponseRedirect("/workspace")
            employes = WorkBio.objects.filter(department=u.workbio.department, role=2)
            task_form = taskForm(request.POST)
            context = {'title': 'Добавление новой задачи',
                       'myUser': u,
                       'task_form': task_form,
                       'employes': employes,
                       'VERSION_SYSTEM': settings.VERSION_SYSTEM}
            return render(request, 'default/mainPages/new-task.html', context)
        else:
            if task_form.is_valid():
                new_task = task_form.save(commit=False)
                new_task.userInitial = u
                new_task.userAssign = u
                new_task.briefDescr = new_task.description[0:50]
                new_task.briefDescr.strip()
                new_task.briefDescr = new_task.briefDescr + '...'
                new_task.date_added = datetime.datetime.now()
                new_task.save()
                return HttpResponseRedirect("/workspace")
            task_form = taskForm(request.POST)
            context = {'title': 'Добавление новой задачи',
                       'myUser': u,
                       'task_form': task_form,
                       'VERSION_SYSTEM':settings.VERSION_SYSTEM}
            return render(request, 'default/mainPages/new-task.html', context)
    elif request.method == 'GET':
        if 'id' in request.GET and request.GET['id'].isdigit:
            if u.workbio.role.id == 1:
                id_get = request.GET['id']
                employes = WorkBio.objects.filter(user=User.objects.get(id=id_get))
                task_form = taskForm()
                context = {'title': 'Добавление новой задачи', 'myUser': u, 'task_form': task_form,
                           'employes': employes,
                           'VERSION_SYSTEM': settings.VERSION_SYSTEM,
                           'user_choised': True}
                return render(request, 'default/mainPages/new-task.html', context)
            else:
                raise Http404
    employes = WorkBio.objects.filter(department=u.workbio.department,role=2)
    task_form = taskForm()
    context = {'title':'Добавление новой задачи',
               'myUser':u,
               'task_form':task_form,
               'employes':employes,
               'VERSION_SYSTEM':settings.VERSION_SYSTEM}
    return render(request,'default/mainPages/new-task.html',context)

@login_required
def userNew(request):
    u = request.user
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        workbio_form = workBioForm(request.POST)
        if user_form.is_valid() and workbio_form.is_valid():
            # Create a new user object but avoid saving it yet
            new_user = user_form.save(commit=False)
            # Save the User object
            new_user.set_password(user_form.cleaned_data['password'])
            new_user.save()
            workbio = workbio_form.save(commit=False)
            workbio.user = new_user
            workbio.department = u.workbio.department
            workbio.role = Role.objects.get(id=2)
            workbio.save()
            return HttpResponseRedirect('/workspace')
        else:
            user_form = UserRegistrationForm(request.POST)
            workbio_form = workBioForm(request.POST)
    else:
        user_form = UserRegistrationForm()
        workbio_form = workBioForm()
    context = {'title':'Создание учётной записи','myUser':u,'user_form':user_form,'workbio_form':workbio_form,'VERSION_SYSTEM':settings.VERSION_SYSTEM}
    return render(request, 'default/mainPages/new-user.html',context)

@login_required
def tasksHistory(request,user_id=False,page=1):
    u = request.user
    if user_id and user_id.isdecimal():
        user_get = User.objects.get(id=int(user_id))
        if u.workbio.role.id == 1 and user_get.workbio.department == u.workbio.department and user_get.is_active:
            user_get = User.objects.get(id=user_id)
            if request.method == 'GET':
                if 'page' in request.GET:
                    page = int(request.GET['page'])
                tasks = Task.objects.order_by('status', '-priorety', '-date_added', '-date_deadline').filter(
                    userAssign=user_get)
            p = Paginator(tasks, 10)
            try:
                tasks = p.page(page)
            except PageNotAnInteger:
                # Если переменная не integet, то перебрасывает на первую страницу
                tasks = p.page(1)
            except EmptyPage:
                # Если страница вне диапазона (например 9999), поставить последнюю страницу результатов.
                tasks = p.page(p.num_pages)
            context = {'title': 'История задач', 'myUser': u, 'tasks': tasks, 'act': 'history', 'user_get': user_get,'VERSION_SYSTEM':settings.VERSION_SYSTEM}
            return render(request, 'default/mainPages/task-list.html', context)
        else:
            return Http404
    else:
        return Http404

@login_required
def taskDelete(request,task=None):
    if task is not None and task.isdigit:
        task_obj = Task.objects.get(id=task)
        if task_obj.status.id == 1 and task_obj.userInitial.workbio.department == request.user.workbio.department and request.user.workbio.role.id == 1:
            task_obj.delete()
    return HttpResponseRedirect("/workspace")