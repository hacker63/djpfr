import datetime

from django.contrib.auth.models import User, AbstractUser
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone

from django.contrib.auth.models import User



def get_first_name(self):
    return '%s %s' % (self.last_name,self.first_name)

User.add_to_class("__str__", get_first_name)


# Create your models here.
class Department(models.Model):
    DepartmentName = models.CharField(max_length=200)
    def __str__(self):
        return self.DepartmentName

class Post(models.Model):
    department = models.ForeignKey(Department,null=True)
    PostName = models.CharField(max_length=50,null=True)
    def __str__(self):
        return self.PostName

class Role(models.Model):
    id = models.AutoField(primary_key=True)
    nameRole = models.CharField(max_length=50,default=None)
    def __str__(self):
        return self.nameRole

class WorkBio(models.Model):
    user = models.OneToOneField(User,primary_key=True,default=0)
    middleName = models.CharField(max_length=50,null=True,verbose_name='Отчество')
    department = models.ForeignKey(Department,null=True,blank=True,default=None,verbose_name='Отдел')
    post = models.ForeignKey(Post,blank=True,null=True,verbose_name='Должность')
    role = models.ForeignKey(Role,null=True,default=None)
    def __str__(self):
        return '%s %s' % (self.user.last_name, self.user.first_name)

class Priorety(models.Model):
    id = models.AutoField(primary_key=True)
    namePriorety = models.CharField(max_length=50)
    def __str__(self):
        return self.namePriorety

class StatusTask(models.Model):
    id = models.AutoField(primary_key=True)
    nameStatus = models.CharField(max_length=50)
    def __str__(self):
        return self.nameStatus

class Task(models.Model):
    id = models.AutoField(primary_key=True)
    nameTask = models.CharField(max_length=100,verbose_name='Название задачи')
    briefDescr = models.CharField(max_length=150,verbose_name='Краткое описание')
    description = models.TextField(max_length=2000,verbose_name='Полное описание задачи')
    date_added = models.DateTimeField(auto_now_add=True,verbose_name='Дата добавления')
    date_closed = models.DateTimeField(blank=True,null=True, verbose_name='Дата закрытия задачи')
    date_deadline = models.DateField(blank=True, null=True,verbose_name='Крайний срок выполнения')
    status = models.ForeignKey(StatusTask, default=1,verbose_name='Статус задачи')
    comment_return = models.CharField(max_length=100, default=None, blank=True, null=True, verbose_name='Комментарий задачи')
    priorety = models.ForeignKey(Priorety, default=5, verbose_name='Приоритет')
    userInitial = models.ForeignKey(User, null=True, related_name='+',verbose_name='Инициатор')
    userAssign = models.ForeignKey(User,null=True,related_name='+',verbose_name='Исполнитель')
    def __str__(self):
         return self.nameTask

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        WorkBio.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.workbio.save()