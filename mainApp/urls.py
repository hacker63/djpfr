"""djPFR URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from . import views

urlpatterns = [
    # ex: /polls/
    # url(r'^$', views.index, name='index'),
    # ex: /polls/5/
    # url(r'^(?P<question_id>[0-9]+)/$', views.detail, name='detail'),
    # ex: /polls/5/results/
    # url(r'^(?P<question_id>[0-9]+)/results/$', views.results, name='results'),
    # ex: /polls/5/vote/
    # url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
    url(r'^$', views.index, name='index'),
    url(r'^index', views.index, name='index'),
    url(r'^workspace', views.workspace, name='workspace'),
    url(r'^my_workspace', views.my_workspace, name='my_workspace'),
    url(r'^users/new', views.userNew, name='userNew'),
    url(r'^users/(?P<user>\d+)/edit', views.userEdit, name='userEdit'),
    url(r'^users/(?P<user>\d+)$', views.user, name='user'),
    url(r'^users', views.users, name='users'),
    url(r'^tasks/new', views.taskNew, name='taskNew'),
    url(r'^tasks/history/(?P<user_id>\d+)$', views.tasksHistory, name='tasksHistory'),
    url(r'^tasks/(?P<task>\d+)/edit', views.taskEdit, name='taskEdit'),
    url(r'^tasks/(?P<task>\d+)/delete', views.taskDelete, name='taskDelete'),
    url(r'^tasks/(?P<task>\d+)$', views.task, name='task'),
    url(r'^tasks', views.tasks, name='tasks'),
]